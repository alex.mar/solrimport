<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class WorkerCommand extends Command
{
    protected static $defaultName = 'app:import-topic';

    protected function configure(): void
    {
        $this
            ->setName('app:import-topic')
            ->setDescription('Import topic to solr');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->write('start');

        return Command::SUCCESS;
    }
}